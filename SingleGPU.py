from keras.models import Sequential
from keras.utils import multi_gpu_model
import keras
from ptdutils import datasetuils
import os
import multiprocessing as threading
import multiprocessing as queue

if __name__ == '__main__':
    config = {
        'epochs': 200,
        'batchsize': 50,
        'imgsize': 100,
        'dataset_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'datasetZX'),
        'dataset_img_names': [],
        'dataset_img_labels': [],
        'TrGen': None,
        'TsGen': None,
        'xTr': [],
        'xTs': [],
        'yTr': [],
        'yTs': [],
        'numGpus': 2,
    }

    config['dataset_img_names'], config['dataset_img_labels'] = datasetuils.load_img_names(config['dataset_path'])
    print('Loaded image names from: {}'.format(config['dataset_path']))
    config['TrGen'], config['TsGen'], config['xTr'], config['xTs'], config['yTr'], config['yTs'] = datasetuils.prepare_data(
        data=datasetuils.load_data(list_img=config['dataset_img_names'],
                                   DIRECTORY_PATH=config['dataset_path'],
                                   IMGSIZE=config['imgsize'],
                                   USE16BITS=False,),
        labels=datasetuils.adapt_labels(config['dataset_img_labels']),
        BATCHSIZE=config['batchsize'],
        channels_first=True)
    print('Initiated the Dataset Generators for a batchsize of {}'.format(config['batchsize']))
    # with tf.device('/cpu:0'):
    seqmodel = Sequential()
    seqmodel.add(keras.layers.BatchNormalization(input_shape=(3,
                                                              config['imgsize'],
                                                              config['imgsize']
                                                              )))
    seqmodel.add(keras.layers.Conv2D(256, kernel_size=(5, 5), activation='tanh', padding='same'))
    seqmodel.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    seqmodel.add(keras.layers.Dropout(0.2))

    seqmodel.add(keras.layers.BatchNormalization(input_shape=(3,
                                                              config['imgsize'],
                                                              config['imgsize']
                                                              )))
    seqmodel.add(keras.layers.Conv2D(128, kernel_size=(5, 5), activation='relu', padding='same'))
    seqmodel.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    seqmodel.add(keras.layers.Dropout(0.2))

    seqmodel.add(keras.layers.BatchNormalization(input_shape=(3,
                                                              config['imgsize'],
                                                              config['imgsize']
                                                              )))
    seqmodel.add(keras.layers.Conv2D(64, kernel_size=(5, 5), activation='relu', padding='same'))
    seqmodel.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    seqmodel.add(keras.layers.Dropout(0.2))

    seqmodel.add(keras.layers.Flatten())
    seqmodel.add(keras.layers.Dense(64))
    seqmodel.add(keras.layers.Activation('relu'))
    seqmodel.add(keras.layers.Dropout(0.2))
    seqmodel.add(keras.layers.Dense(4))
    seqmodel.add(keras.layers.Activation('softmax'))
    seqmodel.summary()

    seqmodel.compile(loss='sparse_categorical_crossentropy',
                     optimizer=keras.optimizers.Adadelta(),
                     # optimizer=keras.optimizers.Adam(lr=1e-3),
                     metrics=['acc'],
                     context=['gpu(0)'])
    print('The model is compiled')


    history = seqmodel.fit_generator(
        config['TrGen'],
        steps_per_epoch=config['batchsize'],
        epochs=config['epochs'],
        verbose=1,
        validation_data=config['TsGen'],
        validation_steps=config['batchsize'],
        shuffle=True,
        workers=12,
    )


    score = seqmodel.evaluate_generator(config['TsGen'], steps=config['batchsize'], verbose=1)  # eso he cambiado, para ver si viene de aqui
    print("Test loss: {}".format(score[0]))
    print("Test accuracy: {}".format(score[1]))
    seqmodel.save('parallelmodel5.h5')
    print('Model saved!')