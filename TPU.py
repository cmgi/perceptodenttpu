import os
import tensorflow as tf
from tensorflow.contrib import tpu
from tensorflow.contrib.cluster_resolver import TPUClusterResolver
import re
from keras.preprocessing.image import ImageDataGenerator
# from keras.models import Sequential, Model
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Activation, Dropout, Flatten, Dense, BatchNormalization, GlobalAveragePooling2D, Input
import matplotlib.pyplot as plt
from tensorflow.keras import regularizers
from tensorflow.keras import optimizers
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.models import Sequential, Model
from sklearn.model_selection import train_test_split
import cv2
import numpy as np
from keras.utils import np_utils


DIRECTORY_PATH = 'datasetZX'
NBCLASSES = 4
IMGSIZE = 150

EPOCHS = 10
BATCHSIZE = 100

FLAGS = tf.flags.FLAGS

TPU_GRPC_URL = TPUClusterResolver(tpu=[os.environ['TPU_NAME']]).get_master()


print('Variables de entorno:')
print(TPU_GRPC_URL)


def adapt_labels(y):
    labels = []
    for val in y:
        if val == '3':
            tmp = 0
        elif val == '9':
            tmp = 1
        elif val == '15':
            tmp = 2
        else:
            tmp = 3
        labels.append(tmp)

    labels = np_utils.to_categorical(labels, 4)
    return labels

def toFuncModel(seqmodel):
    input_layer = Input(batch_shape=seqmodel.layers[0].input_shape)
    prev_layer = input_layer
    for layer in seqmodel.layers:
        prev_layer = layer(prev_layer)
    funcmodel = Model([input_layer], [prev_layer])
    return funcmodel


def load_data(list_img):
    data = []
    for im_path in list_img:
        im = cv2.imread('{}/{}'.format(DIRECTORY_PATH, im_path))
        im = cv2.resize(im, (IMGSIZE, IMGSIZE))
        data.append(im)
    data = np.array(data).reshape(len(data), IMGSIZE, IMGSIZE, 3)
    data = data.astype('float32')
    data /= 255

    return data

def prepare_data(data, labels):
    x_train, x_test, y_train, y_test = train_test_split(data, labels,
                                                        test_size=0.2,
                                                        random_state=42, shuffle=True)

    aug = ImageDataGenerator(rotation_range=90,
                             width_shift_range=0.1,
                             height_shift_range=0.1,
                             rescale=1. / 255,
                             shear_range=0.1,
                             zoom_range=0.1,
                             horizontal_flip=True,
                             vertical_flip=True,
                             fill_mode='nearest')

    training_generator = aug.flow(x_train, y_train, batch_size=BATCHSIZE)
    testing_generator = aug.flow(x_test, y_test, batch_size=BATCHSIZE)

    return training_generator, testing_generator, x_train, x_test, y_train, y_test

def load_img_names():
    list_images = [f for f in os.listdir(DIRECTORY_PATH) if re.search('jpg|JPG|jpeg|JPEG', f)]
    list_img = []
    y = []
    for f in list_images:
        if f == '.DS_Store' or f == '.goutputstream-J8IETZ':
            pass
        if f == "258-3-6-A":
            continue
        else:
            tmp1, mastication_cycles, tmp2 = f.split("-")
            if (
                    mastication_cycles == '6' or mastication_cycles == '18' or mastication_cycles == '25'
                    or mastication_cycles == '118' or mastication_cycles == '10'):
                continue
            y.append(mastication_cycles)
            list_img.append(f)

    return list_img, y


with tf.Session(TPU_GRPC_URL) as sess:
    print('Iniciando sesion de TPU')
    sess.run(tpu.initialize_system())
    sess.run(tf.global_variables_initializer())
    print('Cargando nombre de imagenes')
    list_img, y = load_img_names()
    print('Lista de nombres de imagenes cargada... OK')
    print('Iniciando la creación del modelo')
    seqmodel = Sequential()
    seqmodel.add(Conv2D(8, kernel_size=(8, 8), activation='relu', input_shape=(IMGSIZE, IMGSIZE, 3),
                          data_format='channels_last'))
    seqmodel.add(Conv2D(16, kernel_size=(8, 8), activation='relu'))
    seqmodel.add(MaxPooling2D(pool_size=(2, 2)))
    # self.model.add(Dropout(0.25))
    seqmodel.add(Conv2D(32, kernel_size=(4, 4), activation='relu'))
    seqmodel.add(MaxPooling2D(pool_size=(2, 2)))
    seqmodel.add(Conv2D(32, kernel_size=(4, 4), activation='relu'))
    seqmodel.add(MaxPooling2D(pool_size=(2, 2)))
    # self.model.add(Dropout(0.25, trainable=True))
    seqmodel.add(Conv2D(32, kernel_size=(4, 4), activation='relu'))
    seqmodel.add(MaxPooling2D(pool_size=(2, 2)))
    # self.model.add(Dropout(0.25, trainable=True))
    seqmodel.add(Conv2D(16, (4, 4), padding='same'))
    seqmodel.add(Activation('relu'))
    # self.model.add(BatchNormalization(trainable=True))
    seqmodel.add(Conv2D(8, (2, 2), padding='same'))
    seqmodel.add(Activation('relu'))
    # self.model.add(BatchNormalization(trainable=True))
    seqmodel.add(MaxPooling2D(pool_size=(2, 2)))
    # self.model.add(Dropout(0.2))  #
    seqmodel.add(Flatten())
    # Fully Connected layers: aqui son los mismos para ambas GPUs
    # self.model.add(Flatten())
    seqmodel.add(Dense(4))
    seqmodel.add(Activation('softmax'))
    seqmodel.summary()
    # print('\n\nCompilando modelo....')
    seqmodel.compile(
        optimizer=tf.train.AdamOptimizer(learning_rate=1e-4),
        loss='categorical_crossentropy',
        metrics=['acc']
    )
    # print('Modelo compilado... OK')
    # funcmodel = toFuncModel(seqmodel)
    tpu_model = tpu.keras_to_tpu_model(
        seqmodel,
        strategy=tpu.TPUDistributionStrategy(TPUClusterResolver(TPU_GRPC_URL)))
    print('Modelo de TPU creado... OK')
    # tpu_model.compile(
    #     optimizer=tf.train.AdamOptimizer(learning_rate=1e-4),
    #     loss='categorical_crossentropy',
    #     metrics=['acc']
    # )
    print('Modelo compilado... OK')
    print('Obteniendo Generadores...')
    y_labels = adapt_labels(y)
    training_generator, testing_generator, x_train, x_test, y_train, y_test = prepare_data(
        data=load_data(list_img=list_img), labels=y_labels)
    print('Se ha obtrenido los Generadores de Dataset... OK')

    config = tpu.tpu_config.RunConfig(
        master=TPU_GRPC_URL,
        model_dir='./',
        tpu_config=tpu.tpu_config.TPUConfig(
            iterations_per_loop=BATCHSIZE,
            num_shards=2
        )

    )


    sess.run(tpu_model.fit_generator(
        training_generator,
        steps_per_epoch=BATCHSIZE,
        epochs=EPOCHS,
        verbose=1,
        validation_data=testing_generator,
        validation_steps=BATCHSIZE,
        shuffle=True
    ))
    # history = tpu_model.fit_generator(
    #     training_generator,
    #     steps_per_epoch=BATCHSIZE,
    #     epochs=EPOCHS,
    #     verbose=1,
    #     validation_data=testing_generator,
    #     validation_steps=BATCHSIZE,
    #     shuffle=True
    # )
    score = tpu_model.evaluate(x_test, y_test)
    print("Test loss: {}".format(score[0]))
    print("Test accuracy: {}".format(score[1]))
    sess.run(tpu.shutdown_system())

print('He terminado')