
def adapt_labels(y):
    labels = []
    for val in y:
        if val == '3':
            tmp = 0
        elif val == '9':
            tmp = 1
        elif val == '15':
            tmp = 2
        else:
            tmp = 3
        labels.append([tmp])
    return labels



def load_data(list_img, DIRECTORY_PATH, IMGSIZE, USE16BITS = False):
    import cv2
    import numpy as np
    data = []
    for im_path in list_img:
        im = cv2.imread('{}/{}'.format(DIRECTORY_PATH, im_path))
        im = cv2.resize(im, (IMGSIZE, IMGSIZE))
        data.append(im)
    data = np.array(data).reshape(len(data), IMGSIZE, IMGSIZE, 3)
    if USE16BITS:
        data = data.astype('float16')
    else:
        data = data.astype('float32')

    return data

def prepare_data(data, labels, BATCHSIZE, usetf=False, channels_first=False):
    from sklearn.model_selection import train_test_split
    import keras
    import multiprocessing
    if usetf:
        from tensorflow.keras.preprocessing.image import ImageDataGenerator
    else:
        # from keras.preprocessing.image import ImageDataGenerator
        from ptdutils.imagegenerators import ImageDataGenerator
    x_train, x_test, y_train, y_test = train_test_split(data, labels,
                                                        test_size=0.2,
                                                        random_state=42, shuffle=True)

    try:
        pool.terminate()
    except:
        pass
    n_process = 6

    pool = multiprocessing.Pool(processes=4)

    if channels_first:
        dformat = 'channels_first'
        x_train = keras.utils.to_channels_first(x_train)
        x_test = keras.utils.to_channels_first(x_test)
        dim_ordering = 'th'
    else:
        dformat = 'channels_last'
        dim_ordering = 'tf'
    aug = ImageDataGenerator(rotation_range=90,
                             width_shift_range=0.1,
                             height_shift_range=0.1,
                             shear_range=0.1,
                             zoom_range=0.1,
                             horizontal_flip=True,
                             vertical_flip=True,
                             fill_mode='nearest',
                             dim_ordering=dim_ordering,
                             pool=pool)
                             # data_format=dformat)
    aug.fit(x_train)
    training_generator = aug.flow(x_train, y_train, batch_size=BATCHSIZE)
    testing_generator = aug.flow(x_test, y_test, batch_size=BATCHSIZE)

    return training_generator, testing_generator, x_train, x_test, y_train, y_test

def load_img_names(DIRECTORY_PATH):
    import os
    import re
    list_images = [f for f in os.listdir(DIRECTORY_PATH) if re.search('jpg|JPG|jpeg|JPEG', f)]
    list_img = []
    y = []
    for f in list_images:
        if f == '.DS_Store' or f == '.goutputstream-J8IETZ':
            pass
        if f == "258-3-6-A":
            continue
        else:
            tmp1, mastication_cycles, tmp2 = f.split("-")
            if (
                    mastication_cycles == '6' or mastication_cycles == '18' or mastication_cycles == '25'
                    or mastication_cycles == '118' or mastication_cycles == '10'):
                continue
            y.append(mastication_cycles)
            list_img.append(f)

    return list_img, y
