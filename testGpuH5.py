import tensorflow as tf
import keras
from ptdutils import datasetuils
import os
import numpy as np

with tf.device('/cpu:0'):
    config = {
        'epochs': 32,
        'batchsize': 100,
        'imgsize': 100,
        'dataset_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'datasetZX'),
        'dataset_img_names': [],
        'dataset_img_labels': [],
        'TrGen': None,
        'TsGen': None,
        'xTr': [],
        'xTs': [],
        'yTr': [],
        'yTs': [],
        'numGpus': 2,
        'h5filename': 'parallelmodel2.h5'
    }

    config['dataset_img_names'], config['dataset_img_labels'] = datasetuils.load_img_names(config['dataset_path'])
    print('Loaded image names from: {}'.format(config['dataset_path']))
    config['TrGen'], config['TsGen'], config['xTr'], config['xTs'], config['yTr'], config['yTs'] = datasetuils.prepare_data(
        data=datasetuils.load_data(list_img=config['dataset_img_names'],
                                   DIRECTORY_PATH=config['dataset_path'],
                                   IMGSIZE=config['imgsize']),
        labels=datasetuils.adapt_labels(config['dataset_img_labels']),
        BATCHSIZE=config['batchsize'], usetf=False)
    print('Initiated the Dataset Generators for a batchsize of {}'.format(config['batchsize']))
    # with tf.keras.utils.CustomObjectScope({'GlorotUniform': tf.keras.initializers.glorot_uniform()}):
    h5model = keras.models.load_model(config['h5filename'])
    assert isinstance(h5model, keras.models.Model)
    print('The model is loaded')

    # score = h5model.evaluate_generator(config['TsGen'], steps=len(config['TsGen']), verbose=1,)
    # print("Generator Test loss: {}".format(score[0]))
    # print("Generator Test accuracy: {}".format(score[1]))
    # print('\n')

    # print('The amount of pictures for the second test is: {}'.format(len(config['xTs'])))
    # prediction = h5model.predict(config['xTs'])
    # print(prediction)

    # for x,y in zip(config['xTs'], config['yTs']):
    #     prediction = h5model.predict(x=np.array(x))
    # #     print('Predicted: {} \t Truth: {}'.format(prediction, y))
    #
    # for b in config['TsGen']:
    #     for i in b[0]:
    #         p = h5model.predict(x=np.array([i]), verbose=1, batch_size=config['batchsize'])[0]
    #         print('Predicted: {}'.format(p.argmax()))
    #         print('\n')

    for i, y in zip(config['xTs'], config['yTs']):
        p = h5model.predict(x=np.array([i])).argmax()
        expected = y[0]
        hit = p == expected
        print('Predicted: {} \t Expected: {} \t Hit: {}'.format(p, expected, hit))
