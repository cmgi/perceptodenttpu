import mxnet as mx
import keras
import numpy as np
import os
import re
import cv2
import umodel
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

def gpu_device(gpu_number=0):
    try:
        _ = mx.nd.array([1, 2, 3], ctx=mx.gpu(gpu_number))
    except mx.MXNetError:
        return None
    return mx.gpu(gpu_number)

print('Chewing-gum segmentation using Keras + MXNET: ', gpu_device())

# Set configuration variles
DATASET_PATH = './datasetBIN/'

names_bw = [f for f in os.listdir(DATASET_PATH) if re.search('bmp|BMP', f)]
sample_names = [x.split('.')[0] for x in names_bw]
names_rgb = [f for f in os.listdir(DATASET_PATH)
             if re.search('jpg|JPG|jpeg|JPEG', f) and f.split('.')[0] in sample_names]
assert len(names_bw) == len(names_rgb)
data_rgb = []
data_bw = []
for x, y in zip(names_rgb, names_bw):
    # xsam = cv2.imread('{}{}'.format(DATASET_PATH, x)).transpose((2, 0, 1))
    xsam = cv2.imread('{}{}'.format(DATASET_PATH, x))
    ysam = cv2.imread('{}{}'.format(DATASET_PATH, y), cv2.IMREAD_GRAYSCALE)
    ysam = cv2.threshold(ysam, 127, 1, cv2.THRESH_BINARY)[1]
    data_rgb.append(xsam)
    data_bw.append(ysam)
data_rgb = np.asarray(data_rgb)
data_bw = np.asarray(data_bw)
data_bw = data_bw.reshape(data_bw.shape[0],256,256,1)
print('Images loaded OK: ', data_rgb.shape, data_bw.shape)


seqmodel = umodel.unet(input_size=(256,256,3))
seqmodel.summary()

history = seqmodel.fit(data_rgb,
                       data_bw,
                       epochs=4,
                       steps_per_epoch=30,
                       verbose=1,
                      )
