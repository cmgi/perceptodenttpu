import tensorflow as tf
import keras
from ptdutils import datasetuils
import os
import numpy as np

with tf.device('/cpu:0'):
    config = {
        'epochs': 32,
        'batchsize': 100,
        'imgsize': 120,
        'dataset_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'datasetZX'),
        'dataset_img_names': [],
        'dataset_img_labels': [],
        'TrGen': None,
        'TsGen': None,
        'xTr': [],
        'xTs': [],
        'yTr': [],
        'yTs': [],
        'numGpus': 2,
        'h5filename': 'tpumodel4.h5'
    }

    config['dataset_img_names'], config['dataset_img_labels'] = datasetuils.load_img_names(config['dataset_path'])
    print('Loaded image names from: {}'.format(config['dataset_path']))
    config['TrGen'], config['TsGen'], config['xTr'], config['xTs'], config['yTr'], config['yTs'] = datasetuils.prepare_data(
        data=datasetuils.load_data(list_img=config['dataset_img_names'],
                                   DIRECTORY_PATH=config['dataset_path'],
                                   IMGSIZE=config['imgsize']),
        labels=datasetuils.adapt_labels(config['dataset_img_labels']),
        BATCHSIZE=config['batchsize'], usetf=True)
    print('Initiated the Dataset Generators for a batchsize of {}'.format(config['batchsize']))
    print(len(config['TsGen']))
    with tf.keras.utils.CustomObjectScope({'GlorotUniform': tf.keras.initializers.glorot_uniform()}):
        h5model = tf.keras.models.load_model(config['h5filename'])
        assert isinstance(h5model, tf.keras.models.Model)
        h5model.compile(loss='sparse_categorical_crossentropy', optimizer=tf.keras.optimizers.Adam(lr=1e-3), metrics=['acc'])
        print('The model is compiled')

        # score = h5model.evaluate_generator(config['TsGen'], steps=len(config['TsGen']), verbose=1,)
        # print("Generator Test loss: {}".format(score[0]))
        # print("Generator Test accuracy: {}".format(score[1]))
        # print('\n')

        for i, y in zip(config['xTs'], config['yTs']):
            p = h5model.predict(x=np.array([i])).argmax()
            expected = y[0]
            hit = p == expected
            print('Predicted: {} \t Expected: {} \t Hit: {}'.format(p, expected, hit))

        # for i in config['TsGen']:
        #     score = h5model.evaluate(x=i[0], y=i[1], verbose=1)
        #     print("Test loss: {}".format(score[0]))
        #     print("Test accuracy: {}".format(score[1]))
        #     print('\n')

