import os
import mxnet as mx
import numpy as np
from datetime import datetime, timedelta


def parsen_estimate(pixarray, SIGMA):
    PI = 3.14159
    N = pixarray.shape[0]
    # return [(mx.nd.sum(mx.nd.exp(-mx.nd.power(mx.nd.norm(pixarray-pixarray[i], axis=1), 2)/(2 * SIGMA * SIGMA))))/(2*PI*SIGMA*SIGMA*N) for i in range(N)]
    return [(mx.nd.exp(-mx.nd.power(mx.nd.norm(pixarray-pixarray[i], axis=1), 2)/(2 * SIGMA * SIGMA))).asnumpy().sum()/(2*PI*SIGMA*SIGMA*N) for i in range(10)]





SIGMA = 4
TAU = 20


#
A = mx.nd.array([[0, 1, 2], [4, 5, 6], [11, 43, 66], [32, 5, 8], [9, 44, 23]])
# N = A.shape[0]
# print(A)
# res = [(1/(2*PI*SIGMA*SIGMA*N))*(mx.nd.sum(mx.nd.exp(-mx.nd.power(mx.nd.norm(A-A[i], axis=1), 2)/(2 * SIGMA * SIGMA)))) for i in range(N)]
# print(res)



t1 = datetime.now()
IMGNAME = '000-3-B.jpg'
DATASET_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'datasetZX')
IMG_PATH = os.path.join(DATASET_PATH, IMGNAME)
sample = mx.image.imread(IMG_PATH)
assert isinstance(sample, mx.ndarray.ndarray.NDArray)
w = sample.shape[0]
h = sample.shape[1]
print('The image size is: {}'.format(sample.shape))
N = w * h
print('The amount of pixels is: {}'.format(N))
flattened = sample.reshape((N, 3))
# flattened = flattened / 255
print('The flattened image shape is {}'.format(flattened.shape))

p = parsen_estimate(flattened, SIGMA)
print('Parzen Estimate Shape: {}'.format(p))


t2 = datetime.now()
print('Execution finished at {}'.format((t2-t1).seconds))
print('Parzen Result :')

#
#
